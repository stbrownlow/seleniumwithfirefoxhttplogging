##
import time
from selenium import webdriver

python_lib = "C:/Python27/Lib/site-packages/"

class firefox_http_logger(object):
    def __init__(self, driver):
        self.driver = driver
        self.is_enabled = False
        
        self.driver.command_executor._commands['enableHttpLogger']          = ( "GET",  "/session/$sessionId/http_logger/enable" )
        self.driver.command_executor._commands['disableHttpLogger']         = ( "GET",  "/session/$sessionId/http_logger/disable" )
        self.driver.command_executor._commands['setDebugModeHttpLogger']    = ( "POST", "/session/$sessionId/http_logger/setDebugMode" )
        self.driver.command_executor._commands['addCheckUrlHttpLogger']     = ( "POST", "/session/$sessionId/http_logger/addCheckUrl" )
        self.driver.command_executor._commands['removeCheckUrlHttpLogger']  = ( "POST", "/session/$sessionId/http_logger/removeCheckUrl" )
        self.driver.command_executor._commands['setCheckUrlListHttpLogger'] = ( "POST", "/session/$sessionId/http_logger/setCheckUrlList" )
        self.driver.command_executor._commands['getCheckUrlListHttpLogger'] = ( "GET",  "/session/$sessionId/http_logger/getCheckUrlList" )
        self.driver.command_executor._commands['getHttpLogger']             = ( "POST", "/session/$sessionId/http_logger/getLog" )
        self.driver.command_executor._commands['clearHttpLogger']           = ( "GET",  "/session/$sessionId/http_logger/clearLog" )
        
        self.cached_log = {}
        
    def enable(self):
        self.is_enabled = True
        return self.driver.execute( 'enableHttpLogger' )['value']
    
    def disable(self):
        self.is_enabled = False
        return self.driver.execute( 'disableHttpLogger' )['value']
    
    def setDebugMode(self, t_or_f):
        assert isinstance(t_or_f, bool), "found t_or_f type of '{}'".format(type(t_or_f))
        return self.driver.execute( 'setDebugModeHttpLogger', {'t_or_f': t_or_f} )['value']
    
    # checking is case insensitive
    # use url value of '*' to capture all http responses
    def addCheckUrl(self, url):
        assert isinstance(url, basestring), "found url type of '{}'".format(type(url))
        return self.driver.execute( 'addCheckUrlHttpLogger', {'url' : url} )['value']
    
    def removeCheckUrl(self, url, raise_on_missing=True):
        assert isinstance(url, basestring), "found url type of '{}'".format(type(url))
        assert isinstance(raise_on_missing, bool), "found raise_on_missing type of '{}'".format(type(raise_on_missing))
        
        url_was_removed = self.driver.execute( 'removeCheckUrlHttpLogger', {'url' : url} )['value']
        
        if raise_on_missing and not url_was_removed:
            raise Exception( "url of '{}' was not found in check_url_list".format( url ) )
    
    def setCheckUrlList(self, url_list):
        try:
            assert isinstance(url_list, list) or isinstance(url_list, tuple), "found url_list type of '{}'".format(type(url_list))
        except AssertionError:
            if isinstance(url_list, set):
                url_list = list(url_list)
            else:
                raise
            
        for url in url_list:
            assert isinstance(url, basestring), "found '{}' of type '{}' in url_list".format(url, type(url))
        
        return self.driver.execute( 'setCheckUrlListHttpLogger', {'url_list' : url_list} )['value']
    
    def getCheckUrlList(self):
        return self.driver.execute( 'getCheckUrlListHttpLogger' )['value']
    
    # getting will also clear the window's log
    def getLog(self, window_handle=None):
        if window_handle is None:
            #defaults to currently focused window
            return_log = self.driver.execute( 'getHttpLogger' )['value']
        else:
            return_log = self.driver.execute( 'getHttpLogger', {'window_handle' : window_handle} )['value']
        
        if return_log is None:
            return None
        
        needs_debug = []
        for log_dict in return_log:
            if ('debug' in log_dict) and (not log_dict in needs_debug):
                needs_debug.append( log_dict )
        
        if needs_debug:
            print "when fetching the log, found these records with debug attribute:\n\n{}".format(needs_debug)
        
        return return_log
    
    # completely wipe all logs
    def clearLog(self):
        return self.driver.execute( 'clearHttpLogger' )['value']
    
    
    ##
    # helper functions
    ##
    
    def cacheLog(self, window_handle=None, this_log=None):
        if this_log is None:
            this_log = self.getLog( window_handle=window_handle )
        
        if window_handle is None:
            window_handle = self.driver.current_window_handle
        
        if not window_handle in self.cached_log:
            self.cached_log[window_handle] = []
        
        self.cached_log[window_handle] += this_log
    
    def getLogEntryAndAssertSingle(self, window_handle=None, filterFunction=None, postFilterFunction=None):
        i = 0
        while True:
            if i > 15:
                raise Exception( "timed out waiting for non-empty log" )
            
            log = self.getLog( window_handle=window_handle )
            
            if not filterFunction is None:
                log = [ elem for elem in log if filterFunction(elem) ]
            
            if not postFilterFunction is None:
                log = postFilterFunction( log )
            
            if len(log) == 1:
                return log[0]
            elif log:
                self.cacheLog( window_handle=window_handle, this_log=log )
                if window_handle is None:
                    window_handle = self.driver.current_window_handle
                
                raise Exception( "found {} logs, which have been appended to self.driver.http_logger.cached_log['{}']".format(len(log), window_handle) )
            
            i += 1
            time.sleep( 1 )

def getDriver():
    ff_prof = webdriver.FirefoxProfile()
    ff_prof.set_preference( "extensions.lastAppVersion","17.0.1" )
    ff_bin = webdriver.firefox.firefox_binary.FirefoxBinary( firefox_path=(python_lib + "selenium/firefox17/firefox.exe") )

    ff_prof.set_preference( "browser.tabs.loadInBackground", False )
    ff_prof.set_preference( "browser.tabs.loadDivertedInBackground", False )
    ff_prof.set_preference( "browser.tabs.loadBookmarksInBackground", False )

    ff_prof.set_preference( "toolkit.startup.max_resumed_crashes", -1 )
    ff_prof.set_preference( "browser.formfill.enable", False )
    ff_prof.set_preference( "services.sync.prefs.sync.browser.formfill.enable", False )
    ff_prof.set_preference( "browser.shell.checkDefaultBrowser", False )

    driver = webdriver.Firefox( firefox_binary=ff_bin, firefox_profile=ff_prof )
    driver.http_logger = firefox_http_logger( driver )
    return driver

"""
driver = getDriver()
driver.http_logger.enable()
driver.http_logger.addCheckUrl("*")
driver.get("http://google.com")
http_log = driver.http_logger.getLog()
"""

##