var bigFatEval = {
	execEval: function (eval_this) {
		return eval(eval_this);
	},
}


FirefoxDriver.prototype.bigFatEval = function(a, b) {
    if (b && b.eval_this) {
		a.value = bigFatEval.execEval(b.eval_this);
		a.send();
	} else {
		throw new Error("no eval_this specified").stack;
	}
};
