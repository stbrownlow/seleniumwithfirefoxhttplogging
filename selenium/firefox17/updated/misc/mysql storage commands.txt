for every insertion, I need to check that the record has not previously been scraped

inserted values must be a tuple (list with commas)

#after checking that post does not exist (by id)
"INSERT INTO posts (id, number, memberid, birthday, subject, content, private, ideaid) VALUES (%i,%i,%i,%s,%s,%s,%i,%i)" % (self.postsIdList[i], self.postsNumList[i], MEMBERid, self.postsBirthdayList[i], self.postsSubjectList[i], self.postsContentList[i], self.postsPrivateList[i], IDEAID)

#after checking if member does not exist (by search for memberName)
"INSERT INTO members (name, birthday) VALUES (%s,%s)" % (self.membersNameList[i], DATEFROMIDEA)

#after checking that idea does not exist (by id)
"INSERT INTO ideas (id, birthday, ticker, company, url, returnpercent, memberid, blurb, hidden) VALUES (%i,%s,%s,%s,%s,%i,%i,%s,%i)" % (self.ideasIdList[i], self.ideasBirthdayList[i], self.ideasTickerList[i], self.ideasCompanyList[i], self.ideasUrlList[i], self.ideasReturnList[i], MEMBERID, self.ideasBlurbList[i], self.ideasHiddenList[i])

#if idea is not hidden, then scrape it
"UPDATE ideas SET price=%f, outstandingshares=%i, cap=%i, netdebt=%i, tev=%i, report=%s, catalyst=%s WHERE id=IDEAID[i]" % (self.ideasFigs[0], self.ideasFigs[1], self.ideasFigs[2], self.ideasFigs[3], self.ideasFigs[4], self.ideasReport, self.ideasCatalyst)

