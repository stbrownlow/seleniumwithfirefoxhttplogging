from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class Temp(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://www.valueinvestorsclub.com/"
        self.verificationErrors = []
    
    def test_temp(self):
        driver = self.driver
        driver.get("http://www.valueinvestorsclub.com/value2/Idea/AdvancedSearch")
        driver.find_element_by_id("highEntryDate").clear()
        driver.find_element_by_id("highEntryDate").send_keys("01/01/2008")
        driver.find_element_by_id("search").click()
        driver.find_element_by_css_selector("img[alt=\"Sort Descending\"]").click()
        # ERROR: Caught exception [unknown command []]
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
