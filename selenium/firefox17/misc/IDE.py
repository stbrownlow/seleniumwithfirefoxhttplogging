from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class TestCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://www.valueinvestorsclub.com/"
        self.verificationErrors = []
    
    def test_case(self):
        startDate = "01/01/2001"
        endDate = "12/01/2012"
        loginId = "stbrwnlw@memphis.edu"
        loginPass = "newyorkcity"
    
        driver = self.driver
        driver.get(self.base_url + "/value2/Account/MemberLogin") # open login page
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys(loginId) #enter login info
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys(loginPass)
        driver.find_element_by_id("rememberMe").click()
        driver.find_element_by_id("submit").click() #login
        
        #need an if in case of login pop-up
        driver.find_element_by_id("Month6Bearish").click() #take care of pop-up
        driver.find_element_by_id("Submit").click() #take care of pop-up
        
        driver.get(self.base_url + "/value2/Idea/AdvancedSearch") #open search page
        driver.find_element_by_id("lowEntryDate").clear()
        driver.find_element_by_id("lowEntryDate").send_keys(startDate) #set starting date
        driver.find_element_by_id("highEntryDate").clear()
        driver.find_element_by_id("highEntryDate").send_keys(endDate) #set ending date
        driver.find_element_by_id("search").click() #search by date
        
        #flip search results so that old -> recent
        
        driver.find_element_by_css_selector("div.next.pagerLink > form > div > div").click() #next page
        
        #need test case for when on last page
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
