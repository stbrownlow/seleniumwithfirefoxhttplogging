def startingMySQL(self):
    import MySQLdb #as mdb
    db = MySQLdb.connect(host="localhost", user="root", passwd="password", db="database")
    
    #code
    db.close()
    
def defaultCursorExample(self): #performs CRUD operations (create, read, update, delete)
    cur = db.cursor()
    
    #remember, the element structure is foobarred
    
    cur.execute("CREATE TABLE IF NOT EXISTS \
        People(Id INT PRIMARY KEY AUTO_INCREMENT, Name VARCHAR(25))")
    
    #add an element
    cur.execute("INSERT INTO People(Name) VALUES('Jack London')")
    
    # default cursor example
    #select all data from the People table
    cur.execute("SELECT * FROM People")
    
    #fetchall gets all records. 
    #It returns a result set that is technically a tuple of tuples
    #Each inner tuple represents a row in the table
    rows = cur.fetchall()
    for row in rows:
        print row
    #
    
    # another default cursor example
    cur.execute("SELECT * FROM People")
    
    numrows = int(cur.rowcount)
    for i in range(numrows):
        row = cur.fetchone()
        print row[0], row[1]
    #
    
    for row in cur.fetchall(): #this simply loops through the entire name database, accessing the respective elements
        firstname = str(row[0]) #row is an array
        lastname = str(row[1])
        cur.execute("SELECT firstname, lastname FROM db.Name")
   
    cur.close()
    
def dictCursorExample(self):

    cur = db.cursor(MySQLdb.cursors.DictCursor)
    
    #create the People table
    
    cur.execute("SELECT * FROM People")
    
    rows = cur.fetchall()
    
    for row in rows:
        print "%s %s" % (row["Id"], row["NAME"])
        
def getColumnHeader(self):
    desc = cur.description
    
    #header of row 1 is
    print desc[0][0]
    
    #header of row 2 is
    print desc[1][0]  #according to the site, my intuition says these are flopped
    
def changeElement(self):
    cur.execute("UPDATE People SET Name = %s WHERE Id = %s,
        ("George Washington", "1"))