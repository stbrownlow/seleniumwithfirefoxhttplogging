.

This was coded for Windows running python 2.7, selenium 2.39.0, and firefox 17.0.1

Copy firefox_driver.py and the selenium directory to C:/Python27/Lib/site-packages/

I added this logging a while back, so my memory is a little fuzzy.
But the gist of my changes are:
	* added an http_logger class to the driver (see "~/firefox_driver.py")
	* added code to driver_component.js, which can be found in "~/selenium/webdriver/firefox/webdriver.xpi" (xpi`s are just renamed zip files)
		+ you can find my edits to driver_component.js by doing a ctrl-f for the string "ctrl-f"
		+ there was also another file I had to make a change to (recompile it in some way), but I forget the specifics

.